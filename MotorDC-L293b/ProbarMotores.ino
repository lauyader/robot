//Testeando Motores con L293D
//Definimos pins
//Motor A
int enableA = 5;
int motorA1 = 6;
int motorA2 = 7;
//Motor B
int enableB = 8;
int motorB1 = 9;
int motorB2 = 10;

int led = 13;


void setup() {
  
  Serial.begin (9600);
  //configuración
  pinMode (enableA, OUTPUT);
  pinMode (motorA1, OUTPUT);
  pinMode (motorA2, OUTPUT);  
  
  pinMode (enableB, OUTPUT);
  pinMode (motorB1, OUTPUT);
  pinMode (motorB2, OUTPUT);  
  pinMode(LED_BUILTIN, OUTPUT);
  
}
void loop() {
  //activamos motor A
  Serial.println("Activamos motores");
  digitalWrite (enableA, HIGH);
  digitalWrite (enableB, HIGH);
  digitalWrite(LED_BUILTIN, HIGH);
  delay (1000);
  digitalWrite(LED_BUILTIN, LOW);
  
  //Nos movemos
  Serial.println ("Hacia delante");
  digitalWrite (motorA1, LOW);
  digitalWrite (motorA2, HIGH);
  digitalWrite (motorB1, LOW);
  digitalWrite (motorB2, HIGH);
  //Durante 3 segundos
  delay (3000);
  
  Serial.println ("Hacia atrás");
  digitalWrite (motorA1,HIGH);
  digitalWrite (motorA2,LOW);  
  digitalWrite (motorB1,HIGH);
  digitalWrite (motorB2,LOW);  
  //Durante 3 segundos
  delay (3000);
  Serial.println ("Paramos motores");
  //stop
  digitalWrite (enableA, LOW);
  digitalWrite (enableB, LOW);
  delay (3000);
}