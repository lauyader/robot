#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Interfaz, Panel de control Robot Tortuga
# Adaptaciones: Luis Americo Auyadermont

from Tkinter import *


#Característica de la Ventana

master = Tk()

master.title("Robot Tortuga")
master.geometry("640x480+0+0") #Tamaño de la ventana
master.maxsize(640,480)
master.config(bg="white") # Configuramaos el fondo de la ventana

#Agregamos una imagen al fondo de la pantalla
fondo = PhotoImage(file= "img/fondo.png")
lblFondo=Label(master, image=fondo).place(x=0,y=0)




# Presentacion de los valores en la consola
## Declaraciones de las funciones :

# funcion de control de  velocidad a través de un slider
def velocidad(self):
	print vel.get()

# Obtener valores del sensor  ultrasonido

def ultrasonido(self):
	pass



#Control de Movimientos del robot

def izq():
	print "Girar a la Izquierda"

def der():
	print "Girar a la Derecha"

def avanzar():
	print "Avanzar"

def retroceder():
	print "Retroceder"		

def parar():
	print "Para el Robot"
 
# Control de activación del lapiz

def subirLapiz():
	pass

def bajarLapiz():
	pass


#Creación deel slider para controlar la velocidad
vel = Scale(master, from_=0, to=100, command=velocidad)
#Posicionamiento del widget de control de velocidad (slider)
vel.pack(side=LEFT)
vel.place(x=550, y=100)


# Control de Movimiento

##Avanzar
Av=Button(master, text="Avanzar", command=avanzar)
Av.pack()
Av.place(x=300, y=100)

##Para el Robot
Stop= Button(master, text="Parar", command=parar)
Stop.pack()
Stop.place(x=308, y =135)


##Retroceder
Ret=Button(master, text="Retroceder", command=retroceder)
Ret.pack()
Ret.place(x=290, y =170)

## Izquierda
Izq=Button(master, text = "Izquierda", command=izq)
Izq.pack()
Izq.place(x=210, y=135)

## Derecha()
Der=Button(master, text = "Derecha", command=der)
Der.pack()
Der.place(x=380, y = 135)


##fin de Control de Movimiento ##


#Control del lápiz
## Subir el lapiz
Slapiz=Checkbutton(
        master, text="Subir el Lápiz" 
        )
Slapiz.pack()
Slapiz.place(x=190, y = 410)
Blapiz=Checkbutton(
        master, text="Bajar el Lápiz" 
        )
Blapiz.pack()
Blapiz.place(x=390, y = 410)




mainloop()

